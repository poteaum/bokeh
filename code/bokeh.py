from skimage import img_as_float, img_as_ubyte
from skimage.filters import gaussian
import matplotlib.pyplot as plt

def apply_bokeh(image, landmarks=None):
    """
    Applies a Bokeh effect to a given image.
    
    Parameters:
    - image: Input image (numpy array)
    - landmarks: Optional list of landmark coordinates (default is None)
    
    Returns:
    - Image with bokeh (numpy array)
    """

    ###################################################################################
    ### Example of a simple bokeh filter implemented by blurring the whole image.
    ### This is obviously not achieving the expected result and is provided just as an example.
    ### Modify this part by inserting your code.

    # Convert image to float
    image = img_as_float(image)

    sigma = 3
    bokeh_image = gaussian(image, sigma=sigma, channel_axis=-1)

    # Convert blurred image back to original type
    bokeh_image = img_as_ubyte(bokeh_image)    

    ###################################################################################

    return bokeh_image
import os
from skimage import io
import matplotlib.pyplot as plt
from bokeh import apply_bokeh

def load_landmarks(landmarks_file, image_name):
    """
    Load the landmarks of a corresponding image.
    """
    with open(landmarks_file, 'r') as file:
        for line in file:
            if line.startswith(image_name):
                parts = line.strip().split()
                coords = list(map(int, parts[1:]))
                return coords
    return None


def process_all_images(data_folder, output_folder, landmarks_file):
    """
    Processes all images in the data folder, applies the bokeh filter, 
    and saves the processed images in the output folder.
    
    Parameters:
    - data_folder: Path to the folder containing input images
    - output_folder: Path to the folder where output images will be saved
    - landmarks_file: Path to the file containing image landmarks
    """
    # Create the output folder if it doesn't exist
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    
    # List all image files in the data folder
    image_files = [f for f in os.listdir(data_folder) if f.endswith('.jpg')]
    
    for image_file in image_files:
        image_path = os.path.join(data_folder, image_file)
        
        # Load the image
        image = io.imread(image_path)
        
        # Load the landmarks
        landmarks = load_landmarks(landmarks_file, image_file)
        
        # Apply the bokeh filter
        bokeh_image = apply_bokeh(image, landmarks=landmarks)
        # Save the processed image to the output folder
        output_filename = f"{os.path.splitext(image_file)[0]}_bokeh.jpg"
        output_path = os.path.join(output_folder, output_filename)
        io.imsave(output_path, bokeh_image)
        print(f"Processed and saved: {output_path}")


def show_first_n_images(data_folder, output_folder, N=10):
    """
    Displays the first N images from the data folder and their corresponding processed images from the output folder.
    
    Parameters:
    - data_folder: Path to the folder containing input images
    - output_folder: Path to the folder containing the processed images
    - N: Number of images to display (default is 10)
    """
    # List all image files in the data folder
    image_files = [f for f in os.listdir(data_folder) if f.endswith('.jpg')]
    
    # Determine the number of images to display
    num_images = min(N, len(image_files))
    
    # Create subplots
    fig, axes = plt.subplots(num_images, 2, figsize=(12, 6 * num_images))
    if num_images == 1:
        axes = [axes]
    
    for i, image_file in enumerate(image_files[:num_images]):
        image_path = os.path.join(data_folder, image_file)
        bokeh_image_file = f"{os.path.splitext(image_file)[0]}_bokeh.jpg"

        bokeh_image_path = os.path.join(output_folder, bokeh_image_file)
        
        # Load the original image
        image = io.imread(image_path)
        
        # Load the blurred image
        if os.path.exists(bokeh_image_path):
            bokeh_image = io.imread(bokeh_image_path)
        else:
            print(f"Processed image not found for {image_file}")
            bokeh_image = None
        
        # Display the original and bokeh images side by side
        ax_orig, ax_bokeh = axes[i]
        
        # Original image
        ax_orig.imshow(image)
        ax_orig.set_title(f"Image {i+1}")
        ax_orig.axis('off')
        
        # Blurred image
        if bokeh_image is not None:
            ax_bokeh.imshow(bokeh_image)
            ax_bokeh.axis('off')
        else:
            ax_bokeh.axis('off')
    
    plt.tight_layout()
    plt.show()